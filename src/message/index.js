import React, {Component} from "react";
import {connect} from 'react-redux';
import '../styles/message.css'

class Message extends Component {
    constructor(props) {
        super(props);
        this.state = {isLike: false}
        this.likeMessage = this.likeMessage.bind(this);
    }

    likeMessage() {
        let status = this.state.isLike;
        this.setState({isLike: !status});
    }

    render() {
        let btnClass, iClass;
        if(this.state.isLike) {
            btnClass = 'message-liked';
            iClass = 'fas fa-heart';
        }
        else {
            btnClass = 'message-like';
            iClass = 'far fa-heart';
        }
        return (
            <div className='message d-flex'>
                <img src={this.props.avatar} alt="" className='message-user-avatar'/>
                <div>
                    <div className='message-user-name'>{this.props.user}</div>
                    <div className='message-text'>{this.props.text}</div>
                    <span className='message-time'>
                        {new Date(this.props.createdAt).toTimeString().substr(0,5)}
                    </span>
                    <button className={btnClass} onClick={this.likeMessage}>
                        <i className={iClass}/>
                    </button>
                </div>
            </div>
        );
    }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Message);