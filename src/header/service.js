export const getValues = (messages) => {

    const usersCount = countAmount(messages, 'userId')
    const messagesCount = messages.length;
    const lastMessageDate = maxDate(messages);
    return {
        chatName: 'Chat',
        usersCount,
        messagesCount,
        lastMessageDate
    };
};

function countAmount(messages, key) {
    let buf = [];
    let counter = 0;
    for (let message of messages) {
        if (!buf.includes(message[key])) {
            counter++;
            buf.push(message[key]);
        }
    }
    return counter;
}

function maxDate(messages) {
    let dates = [];
    for (let message of messages) {
        dates.push(new Date(message['createdAt']));
    }
    const maxDate = new Date(Math.max.apply(null, dates));
    let result = '';
    if (maxDate.toDateString() !== new Date().toDateString()) {
        let day = maxDate.getDate().toString(),
            month = (maxDate.getMonth() + 1).toString();
        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;
        result += day + '.' + month;
        if (maxDate.getFullYear() !== new Date().getFullYear()) {
            result += '.' + maxDate.getFullYear();
        }
        result += ' ';
    }
    result += maxDate.getHours() + ':' + maxDate.getMinutes();
    return result;
}