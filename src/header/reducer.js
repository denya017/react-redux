import {GET_HEADER_VALUES} from "./actionTypes";

const initialState = {
    chatName: 'Chat',
    usersCount: 0,
    messagesCount: 0,
    lastMessageDate: ''
};

// eslint-disable-next-line import/no-anonymous-default-export
export default function (state = initialState, action) {
    switch (action.type) {
        case GET_HEADER_VALUES: {
            return action.payload;
        }
        default:
            return state;
    }
}