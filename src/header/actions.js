import {GET_HEADER_VALUES} from "./actionTypes";
import {getValues} from './service'

export const getHeaderValues = (messages) => ({
    type: GET_HEADER_VALUES,
    payload: getValues(messages)
});