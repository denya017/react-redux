import React, {Component} from "react";
import {connect} from 'react-redux';
import * as actions from './actions';
import '../styles/header.css'

class Header extends Component {
    componentDidMount() {
        this.props.getHeaderValues(this.props.messages);
    }

    render() {
        const {title, usersCount, messagesCount, lastMessageDate} = this.props.header;
        return (
            <div className='header navbar shadow'>
                <span className='header-title'>
                    <i className="fas fa-icons me-1"/>
                    {title ? title : 'Chat'}</span>
                <span className='header-users-count'>
                    {usersCount} participants
                    <i className="fas fa-users ms-1"/>
                </span>
                <span className='header-messages-count'>
                    {messagesCount} messages
                    <i className="fas fa-comments ms-1"/>
                </span>
                <span className='header-last-message-date'>
                    <i className="fas fa-history me-1"/>
                    last message at {lastMessageDate}
                </span>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        messages: state.chat.messages,
        header: state.header
    }
};

const mapDispatchToProps = {
    ...actions
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);