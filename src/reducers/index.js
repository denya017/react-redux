import { combineReducers } from "redux";
import chat from "../chat/reducer";
import header from "../header/reducer";

const rootReducer = combineReducers({
    chat,
    header
});

export default rootReducer;