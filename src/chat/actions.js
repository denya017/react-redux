import {DOWNLOAD_MESSAGES} from "./actionTypes";

export const downloadMessages = (data) => ({
    type: DOWNLOAD_MESSAGES,
    payload: {
        preloader: data.preloader,
        messages: data.messages
    }
});

export function fetchMessages(url) {
    return (dispatch) => {
        fetch(url)
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then((data) => {
                dispatch(downloadMessages({
                    preloader: false,
                    messages: data
                }));
                return data;
            })
            .catch(e => {
                console.log(e);
            });
    }
}