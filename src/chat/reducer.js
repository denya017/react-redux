import {DOWNLOAD_MESSAGES} from "./actionTypes";
import {SEND_MESSAGE} from "../messageInput/actionTypes";

const initialState = {
    preloader: true,
    messages: {}
};

// eslint-disable-next-line import/no-anonymous-default-export
export default function (state = initialState, action) {
    switch (action.type) {
        case DOWNLOAD_MESSAGES: {
            return action.payload;
        }
        case SEND_MESSAGE: {
            let result = state;
            result.messages.push(action.payload);
            return result;
        }
        default:
            return state;
    }
}