import React, {Component} from "react";
import {connect} from 'react-redux';
import * as actions from './actions';
import Header from "../header";
import MessageList from "../messageList";
import Preloader from "../preloader";
import MessageInput from "../messageInput";

class Chat extends Component {
    componentDidMount() {
        this.props.fetchMessages(this.props.url);
    }

    render() {
        if (this.props.preloader) {
            return (
                <Preloader/>
            );
        }
        return (
            <div className='chat'>
                <Header/>
                <MessageList/>
                <MessageInput/>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        preloader: state.chat.preloader,
        messages: state.chat.messages
    }
};

const mapDispatchToProps = {
    ...actions
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);