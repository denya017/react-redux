import React, {Component} from "react";
import {connect} from 'react-redux';
import Message from "../message";
import '../styles/common.css'

class MessageList extends Component {

    sortMessages(messages) {
        let sortedMessages = messages;
        sortedMessages.sort((a,b)=> {return (new Date(a['createdAt']).getTime())-(new Date(b['createdAt']).getTime())});
        return sortedMessages;
    }

    getDateMessage(message) {
        const messageDate = new Date(message['createdAt']);
        let date = '';
        let day = messageDate.getDate().toString(),
            month = (messageDate.getMonth() + 1).toString();
        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;
        date += day + '.' + month + '.' + messageDate.getFullYear();
        return date;
    }

    render() {
        const messages = this.sortMessages(this.props.messages);
        let date = '';
        return (
            <div className='message-list'>
                {
                    messages.map((message)=>{
                        let currentDate = this.getDateMessage(message);
                        let dateElement;
                        if(currentDate !== date){
                            date = currentDate;
                            dateElement = <div key={date} id={date} className='date d-flex justify-content-center my-4'><hr className='w-25'/>{date}<hr className='w-25'/></div>;
                        }
                        return [
                            dateElement,
                            <Message
                                key={message.id}
                                avatar={message.avatar}
                                user={message.user}
                                text={message.text}
                                createdAt={message.createdAt}
                            />
                        ];
                    })
                }
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        messages: state.chat.messages
    }
};

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(MessageList);