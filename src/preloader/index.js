import React from 'react';


class Preloader extends React.Component {
    render() {
        return(
            <div className='preloader mt-5 d-flex flex-column align-items-center'>
                <div className="spinner-border" role="status"/>
                ...Loading
            </div>
        );
    }
}

export default Preloader;