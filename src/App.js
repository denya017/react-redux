import Chat from './chat'

function App() {
  return (
    <div className="App">
      <Chat
          url="https://edikdolynskyi.github.io/react_sources/messages.json"
      />
    </div>
  );
}

export default App;
