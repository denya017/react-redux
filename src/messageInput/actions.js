import {SEND_MESSAGE} from "./actionTypes";

export const sendMessage = (data) => ({
    type: SEND_MESSAGE,
    payload: data
});