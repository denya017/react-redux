import React, {Component} from "react";
import {connect} from 'react-redux';
import '../styles/common.css'
import * as actions from "./actions";

class MessageInput extends Component {
    constructor(props) {
        super(props);
        this.state = {textMessage: ''}
        this.send = this.send.bind(this);
    }
    onChange(e) {
        const value = e.target.value;
        this.setState({textMessage: value});
    }
    send = (event) => {
        if (this.state.textMessage !== '') {
            const dateSending = new Date();
            this.props.sendMessage({
                id: dateSending.getTime(),
                userId: "1",
                avatar: "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA",
                user: "Me",
                text: this.state.textMessage,
                createdAt: dateSending.toISOString(),
                editedAt: ""
            });
            this.setState({textMessage: ''});
        }
        event.preventDefault();
    }

    render() {
        return (
            <div className='message-input d-flex'>
                <textarea id="input" className="form-control" rows="3" value={this.state.textMessage} onChange={(e) => this.onChange(e)}/>
                <div className='d-flex align-items-center'>
                    <button type="submit" className="btn btn-primary d-flex flex-nowrap align-items-center ms-4" onClick={this.send}>
                        <i className="fas fa-paper-plane me-2"/>
                        Send
                    </button>
                </div>
            </div>
        );
    }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = {
    ...actions
};

export default connect(mapStateToProps, mapDispatchToProps)(MessageInput);