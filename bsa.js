import Chat from './src/chat';
import rootReducer from './src/reducers';

export default {
    Chat,
    rootReducer,
};